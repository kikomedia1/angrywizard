# Angry Wizard

Hello out there and thank you for checking out our GGJ 2024 game!

## You're in trouble.

You're playing the wizard's apprentice and you messed up. While cleaning, you disposed of a stinky pair of socks that was apparently his favourite one. Now he shut you into a cursed room full of animated objects - and he won't let you out until it's clean.

How long can you last before an enchanted potion turns you into a goat? Or worse... You never know what that grumpy old man is up to.

## Want to give it a shot?

You can try it directly on [GitLab Pages](https://angrywizard-kikomedia1-281c9e018f4275ce1d1bdf2b8aa0166ac6b2d8d3.gitlab.io/)!
