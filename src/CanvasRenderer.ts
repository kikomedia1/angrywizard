import {rectanglesIntersect, spriteToRect} from "./utils.ts";
import {Scaler} from "./Scaler.ts";

export type Sprite = {
  image: CanvasImageSource | undefined,
  x: number,
  y: number,
  width: number,
  height: number,
}

export type Position2D = {
  x: number,
  y: number,
}

export type Line = {
  start : Position2D,
  end : Position2D
}

export class CanvasRenderer {
  private scaler: Scaler;
  private videoElement: HTMLVideoElement;
  private canvasElement: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;
  private spritesMap: Map<string, Sprite>;
  private skeletonLines: Line[] = [];
  private backgroundSprite: Sprite | undefined;
  private hudSprites: Sprite[] = [];
  private registeredFunctions: Array<(passedTime: number) => void>;
  private lastRender: number;

  constructor(scaler: Scaler, stream: MediaStream, video: HTMLVideoElement, canvas: HTMLCanvasElement) {
    this.scaler = scaler;
    this.canvasElement = canvas;
    this.context = canvas.getContext("2d") as CanvasRenderingContext2D;
    //this.context.imageSmoothingEnabled = false;
    //this.context.translate(0.5,0.5)
    this.spritesMap = new Map<string, Sprite>;
    this.registeredFunctions = [];
    this.registerRenderFunction(this.drawBackground.bind(this));
    this.registerRenderFunction(this.drawSprites.bind(this));
    this.registerRenderFunction(this.drawHudSprites.bind(this));
    // this.registerRenderFunction(this.drawLines.bind(this));
    this.lastRender = Date.now();
    this.videoElement = video;
    this.videoElement.srcObject = stream;
    this.setResolution(this.scaler.targetResolution.width, this.scaler.targetResolution.height);
    this.videoElement.play();
  }

  registerRenderFunction(fn: (passedTime: number) => void) {
    this.registeredFunctions.push(fn);
  }

  renderCanvas(){
    this.context.setTransform(-1,0,0,1, this.canvasElement.width, 0);
    this.context.drawImage(this.videoElement, 0, 0, this.canvasElement.width, this.canvasElement.height);
    const now = Date.now();
    const timeDelta = now - this.lastRender;
    for (const registeredFunction of this.registeredFunctions) {
      registeredFunction(timeDelta);
    }
    this.lastRender = now;
    requestAnimationFrame(this.renderCanvas.bind(this));
  }

  setBackground(backgroundSprite: CanvasImageSource){
    this.backgroundSprite = {
      image: backgroundSprite,
      x: 0,
      y: 0,
      width: this.canvasElement.width,
      height: this.canvasElement.height
    }
  }

  drawBackground() {
    if(this.backgroundSprite){
      this.context.drawImage(this.backgroundSprite.image!, this.backgroundSprite.x, this.backgroundSprite.y, this.backgroundSprite. width, this.backgroundSprite.height)
    } else {
      this.context.fillStyle = "black";
      this.context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    }
  }

  clearHudSprites() {
    this.hudSprites = [];
  }

  addHudSprite(hudSprite : Sprite) {
    this.hudSprites.push(hudSprite);
  }

  clearLines() {
    this.skeletonLines = [];
  }

  addLine(x1: number, y1: number, x2: number, y2: number) {
    const startPoint : Position2D = {x: x1, y: y1};
    const endPoint : Position2D = {x: x2, y: y2};
    const newLine : Line = {start: startPoint, end: endPoint};
    this.skeletonLines.push(newLine);
  }

  drawLines() {
    this.context.beginPath();
    this.context.lineWidth = 16 ;
    this.context.strokeStyle = "green";
    for (let l = 0; l < this.skeletonLines.length; l++) {
      this.context.moveTo(this.skeletonLines[l].start.x, this.skeletonLines[l].start.y);
      this.context.lineTo(this.skeletonLines[l].end.x, this.skeletonLines[l].end.y);
    }
    this.context.stroke();
  }
  
  drawHudSprites() {
    for (let h = 0; h < this.hudSprites.length; h++) {
      const hudSprite = this.hudSprites[h];
      this.context.save(); // Save the current state
      this.context.scale(-1,1); // Set scale to flip the image
      this.context.drawImage(hudSprite.image!, hudSprite.x, hudSprite.y, hudSprite. width, hudSprite.height)
      this.context.restore();
    }
  }
  
  drawSprites() {
    for (const [, sprite] of this.spritesMap){
      if(sprite.image){
        this.context.drawImage(sprite.image, sprite.x, sprite.y, sprite.width, sprite.height);
      } else {
        this.context.fillRect(sprite.x, sprite.y, sprite.width, sprite.height);
      }
    }
  }

  addSprite(sprite: Sprite, id?: string): string {
    id = id ? id : new Date().toString();
    this.spritesMap.set(id, sprite);
    return id;
  }

  moveSprite(id: string, x: number, y: number){
    if(this.spritesMap.has(id)){
      const entry = this.spritesMap.get(id) as Sprite;
      this.spritesMap.set(id, {...entry, x: x, y: y})
    } else {
      throw Error("Tried moving a sprite that doesn't exist");
    }
  }

  deleteSprite(id: string){
    if(this.spritesMap.has(id)){
      this.spritesMap.delete(id);
    } else {
      throw Error("Tried deleting a sprite that doesn't exist");
    }
  }

  getOverlappingSprites(id: string): string[] {
    if (this.spritesMap.has(id)) {
      const overlapping: string[] = [];
      const referenceRect = spriteToRect(this.spritesMap.get(id) as Sprite)
      for (const [id, sprite] of this.spritesMap) {
        const rect = spriteToRect(sprite);
        if (rectanglesIntersect(referenceRect, rect)){
          overlapping.push(id);
        }
      }
      return overlapping;
    } else {
      console.log("err")
      throw Error("Sprite doesn't exist")
    }
  }

  tryGetOverlappingSprites(id: string): string[] {
    if (this.spritesMap.has(id)) {
      const overlapping: string[] = [];
      const referenceRect = spriteToRect(this.spritesMap.get(id) as Sprite)
      for (const [id, sprite] of this.spritesMap) {
        const rect = spriteToRect(sprite);
        if (rectanglesIntersect(referenceRect, rect)){
          overlapping.push(id);
        }
      }
      return overlapping;
    } else {
      return [];
    }
  }

  hasSprite(id:string): boolean {
    return this.spritesMap.has(id);
  }

  getSprite(id:string): Sprite {
    return this.spritesMap.get(id)!;
  }

  setResolution(width: number, height: number) {
    if (this.canvasElement) {
      this.canvasElement.width = width;
      this.canvasElement.height = height;
    }
  }

  getResolution(): number[] {
    return [this.canvasElement.width, this.canvasElement.height];
  }
}
