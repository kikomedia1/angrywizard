import {Pose} from "@tensorflow-models/pose-detection";
import {CanvasRenderer} from "./CanvasRenderer.ts";
import {PoseDetectionContainer} from "./PoseDetectionContainer.ts";

enum TargetVariant {
  elixir1,
  elixir2,
  elixir3,
  elixir4,
  mouthAngry
}

type Target = {
  x: number,
  y: number,
  width: number,
  height: number,
  speed: number,
  score: number,
  id: string,
  variant: TargetVariant
}

function randomVariant(): TargetVariant {
  return (Math.floor(Math.random()*4)) as TargetVariant;
}

export class GameManager {
  renderer: CanvasRenderer;
  detectionContainer: PoseDetectionContainer;
  sprites: HTMLImageElement[];
  audioFiles: HTMLAudioElement[];
  score: number;
  maxScore: number =  500;
  spawnInterval: number;
  spawnAngryInterval: number;
  baseSpeed: number;
  currentPose: Pose = {} as Pose;
  targets: Target[] = [];
  leftHandSlotId : string | undefined;
  leftHandVariant : TargetVariant = TargetVariant.elixir1;
  rightHandVariant : TargetVariant = TargetVariant.elixir1;
  rightHandSlotId : string | undefined;
  mouthSlotId : string | undefined;
  volume: number = 0.4;

  constructor(renderer: CanvasRenderer, detectionContainer: PoseDetectionContainer, sprites: HTMLImageElement[], audioFiles: HTMLAudioElement[]) {
    this.renderer = renderer;
    this.renderer.registerRenderFunction(this.moveTargets.bind(this));
    this.renderer.registerRenderFunction(this.calculateHits.bind(this));
    this.renderer.setBackground(sprites[6])
    this.sprites = sprites;
    this.audioFiles = audioFiles;
    this.detectionContainer = detectionContainer;
    this.detectionContainer.setCallback(this.updatePoses.bind(this));
    this.score = 0;
    this.spawnInterval = 0;
    this.spawnAngryInterval = 0;
    this.baseSpeed = 80;
  }

  startGame(){
    this.score = 0;
    const width = this.renderer.getResolution()[0];
    const offset = width / 8;
    this.playAudioEffect(0);
    this.spawnInterval = setInterval(() => {
      const position = Math.random() * (width - offset * 2);
      const speed = this.baseSpeed + this.score;
      this.targets.push({
        x: offset + position,
        y: 0,
        width: 44,
        height: 56,
        speed: speed,
        score: speed / 3,
        id: "elixir" + (new Date().toString()),
        variant: randomVariant()
      })
    }, 1000);

    this.spawnAngryInterval = setInterval(() => {
      const position = Math.random() * (width - offset * 2);
      const speed = this.baseSpeed + this.score;
      this.targets.push({
        x: offset + position,
        y: this.renderer.getResolution()[1],
        width: 96,
        height: 44,
        speed: speed,
        score: speed / 3,
        id: "angry" + (new Date().toString()),
        variant: TargetVariant.mouthAngry
      })
    }, 5000);
  }

  stopGame() {
    clearInterval(this.spawnInterval);
    this.targets = [];
  }

  getElixirSprite(targetVariant: TargetVariant){
    switch (targetVariant) {
      case TargetVariant.elixir1: return this.sprites[7];
      case TargetVariant.elixir2: return this.sprites[8];
      case TargetVariant.elixir3: return this.sprites[9];
      case TargetVariant.elixir4: return this.sprites[10];
      case TargetVariant.mouthAngry: return this.sprites[13];
    }
  }

  getSpriteIdByVariant(targetVariant: TargetVariant) : number{
    switch (targetVariant) {
      case TargetVariant.elixir1: return 7;
      case TargetVariant.elixir2: return 8;
      case TargetVariant.elixir3: return 9;
      case TargetVariant.elixir4: return 10;
      case TargetVariant.mouthAngry: return 13;
    }
  }

  getPowerSprite(power: number){
    return this.sprites[14+power];
  }

  moveTargets(passedTimeInMilliseconds: number){
    for (let i = 0; i < this.targets.length; i++) {
      const target = this.targets[i];
      if(!this.renderer.hasSprite(target.id)){
        this.renderer.addSprite(
          {
            image: this.getElixirSprite(target.variant),
            x: target.x,
            y: target.y,
            width: (target.variant == TargetVariant.mouthAngry) ? 96 : 36,
            height: (target.variant == TargetVariant.mouthAngry) ? 44 : 60,
          },
          target.id,
        )
      } else {
        if (target.variant == TargetVariant.mouthAngry) {
          target.y -= target.speed * passedTimeInMilliseconds / 1000; // CSS Pixels per second
        } else {
          target.y += target.speed * passedTimeInMilliseconds / 1000; // CSS Pixels per second
        }
        this.renderer.moveSprite(target.id, target.x, target.y);
      }
      if(target.y > this.renderer.getResolution()[1] && target.variant != TargetVariant.mouthAngry){
        this.renderer.deleteSprite(target.id);
        this.updateScore(-target.score);
        this.targets.splice(i, 1);
      }
      if(target.y < 0 && target.variant == TargetVariant.mouthAngry){
        this.renderer.deleteSprite(target.id);
        this.updateScore(-target.score);
        this.targets.splice(i, 1);
      }
    }
  }

  calculateHits(){
    try {
      let collisionIDs: string[] = [];
      //collisionIDs = collisionIDs.concat(this.renderer.getOverlappingSprites(this.getIdForKeypoint(0, 0)));
      if (this.rightHandSlotId === undefined) {
        collisionIDs = this.renderer.tryGetOverlappingSprites(this.getIdForKeypoint(0, 9));

        //delete each target with the ids matching the ones in collisions
        for (const collisionID of collisionIDs) {
          this.targets = this.targets.filter(target => {
            const match = target.id === collisionID && target.variant != TargetVariant.mouthAngry;
            if(match){
              this.updateScore(target.score);
              this.renderer.deleteSprite(target.id);
              this.rightHandSlotId = target.id;
              this.rightHandVariant = target.variant;
              this.playGrab()
            }
            return !match;
          });
        }
      }

      if (this.leftHandSlotId === undefined) {
        collisionIDs = this.renderer.tryGetOverlappingSprites(this.getIdForKeypoint(0, 10));

        //delete each target with the ids matching the ones in collisions
        for (const collisionID of collisionIDs) {
          this.targets = this.targets.filter(target => {
            const match = target.id === collisionID && target.variant != TargetVariant.mouthAngry;
            if(match){
              this.updateScore(target.score);
              this.renderer.deleteSprite(target.id);
              this.leftHandSlotId = target.id;
              this.leftHandVariant = target.variant;
              this.playGrab()
            }
            return !match;
          });
        }
      }

      if (this.mouthSlotId === undefined) {
        collisionIDs = this.renderer.tryGetOverlappingSprites(this.getIdForKeypoint(0, 0));

        //delete each target with the ids matching the ones in collisions
        for (const collisionID of collisionIDs) {
          this.targets = this.targets.filter(target => {
            const match = target.id === collisionID && target.variant == TargetVariant.mouthAngry && (target.y < this.currentPose.keypoints[0].y+20);
            if(match){
              //console.log(this.targets);
              this.updateScore(target.score);
              this.renderer.deleteSprite(target.id);
              this.mouthSlotId = target.id;
              setTimeout(() => { this.mouthSlotId = undefined; }, 3000);
              setTimeout(() => { this.playJoke(); }, 10);
            }
            return !match;
          });
        }
      }

    } catch (e) {
      console.log(e);
      // player pose probably not detected yet
      return;
    }
  }

  updatePoses(pose: Pose){
    this.currentPose = pose;
    this.updateSpriteForKeypointId(6, 1, 0, -100, 248, 184);

    const mouthIdentifier = this.getIdForKeypoint(0, 0);
    if(this.renderer.hasSprite(mouthIdentifier)){
      this.renderer.deleteSprite(mouthIdentifier);
    }

    if (this.mouthSlotId) {
      this.updateSpriteForKeypointId(0, 11, -80, -200, 160, 256);
    } else {
      this.updateSpriteForKeypointId(0, 0, -80, -200, 160, 256);
    }
    
    if (this.currentPose.keypoints[9].x > 270*4) {
      if (!(this.rightHandSlotId === undefined)) {
        this.playPlace();
        this.rightHandSlotId = undefined;
      }
    }

    if (this.currentPose.keypoints[10].x < 50*4+56) {
      if (!(this.leftHandSlotId === undefined)) {
        this.playPlace();
        this.leftHandSlotId = undefined;
      }
    }
    const rightHandIdentifier = this.getIdForKeypoint(0, 9);
    if(this.renderer.hasSprite(rightHandIdentifier)){
      this.renderer.deleteSprite(rightHandIdentifier);
    }
    if (this.rightHandSlotId === undefined) {
      this.updateSpriteForKeypointId(9, 5, -80, -200, 72, 72);
      const rightHandSlotSpriteIdentifier = this.getIdForTypeAndIndex("rightHand", 0);
      if (this.renderer.hasSprite(rightHandSlotSpriteIdentifier)) {
        this.renderer.deleteSprite(rightHandSlotSpriteIdentifier);
      }
    } else {
      this.updateSpriteForKeypointId(9, 3, -80, -200, 56, 56);
      const rightHandSlotSpriteIdentifier = this.getIdForTypeAndIndex("rightHand", 0);
      if (this.renderer.hasSprite(rightHandSlotSpriteIdentifier)) {
        this.renderer.deleteSprite(rightHandSlotSpriteIdentifier);
      }
      this.updateSpriteForTypeAndIndex("rightHand", 0, this.getSpriteIdByVariant(this.rightHandVariant), this.currentPose.keypoints[9].x - 80, this.currentPose.keypoints[9].y - 200, 44, 56);
    }

    const leftHandIdentifier = this.getIdForKeypoint(0, 10);
    if(this.renderer.hasSprite(leftHandIdentifier)){
      this.renderer.deleteSprite(leftHandIdentifier);
    }
    if (this.leftHandSlotId === undefined) {
      this.updateSpriteForKeypointId(10, 4, -80, -200, 72, 72);
      const leftHandSlotSpriteIdentifier = this.getIdForTypeAndIndex("leftHand", 0);
      if (this.renderer.hasSprite(leftHandSlotSpriteIdentifier)) {
        this.renderer.deleteSprite(leftHandSlotSpriteIdentifier);
      }
    } else {
      this.updateSpriteForKeypointId(10, 2, -80, -200, 56, 56);
      const leftHandSlotSpriteIdentifier = this.getIdForTypeAndIndex("leftHand", 0);
      if (this.renderer.hasSprite(leftHandSlotSpriteIdentifier)) {
        this.renderer.deleteSprite(leftHandSlotSpriteIdentifier);
      }
     
      this.updateSpriteForTypeAndIndex("leftHand", 0, this.getSpriteIdByVariant(this.leftHandVariant), this.currentPose.keypoints[10].x - 80, this.currentPose.keypoints[10].y - 200, 44, 56);
    }

    const mouthSlotIdentifier = this.getIdForTypeAndIndex("mouth", 0);
    if (this.renderer.hasSprite(mouthSlotIdentifier)) {
      this.renderer.deleteSprite(mouthSlotIdentifier);
    }

    //if (this.mouthSlotId) {
    //  this.updateSpriteForTypeAndIndex("mouth", 0, 12, this.currentPose.keypoints[0].x - 48, this.currentPose.keypoints[0].y + 4, 96, 44);
    //}

    this.updateSkeleton();
    this.updateHud();
  }

  updateScore(delta: number) {
    if(this.score + delta > 0){
      this.score += delta;
    } else {
      this.score = 0;
    }
  }

  addLineByPoseAndKeypoints(startKeypoint: number, endKeypoint: number) {
    this.renderer.addLine(this.currentPose.keypoints[startKeypoint].x, this.currentPose.keypoints[startKeypoint].y,
                          this.currentPose.keypoints[endKeypoint].x, this.currentPose.keypoints[endKeypoint].y,);
  }

  updateSkeleton() {
    this.renderer.clearLines();
    if(this.currentPose) {
      this.addLineByPoseAndKeypoints(7, 9);
      this.addLineByPoseAndKeypoints(5, 7);
      this.addLineByPoseAndKeypoints(5, 6);
      this.addLineByPoseAndKeypoints(6, 8);
      this.addLineByPoseAndKeypoints(8, 10);

      this.addLineByPoseAndKeypoints(0, 1);
      this.addLineByPoseAndKeypoints(0, 2);
      this.addLineByPoseAndKeypoints(4, 2);
      this.addLineByPoseAndKeypoints(3, 1);
    }
  }

  updateHud() {
    this.renderer.clearHudSprites();
    const barSprite = this.sprites[14];
    let power : number = 1;

    if (this.score >= this.maxScore/4) {
      power = 2;
    }
    if (this.score >= this.maxScore/4*2) {
      power = 3;
    }
    if (this.score >= this.maxScore/4*3) {
      power = 4;
    }
    if (this.score >= this.maxScore) {
      power = 5;
    }

    const powerSprite = this.getPowerSprite(power);
    // Bar
    this.renderer.addHudSprite(
      {
        image: barSprite,
        x: -barSprite.width,
        y: 0,
        width: barSprite.width,
        height: barSprite.height,
      } 
    );
    // Power
    this.renderer.addHudSprite(
      {
        image: powerSprite,
        x: -732,
        y: 3,
        width: powerSprite.width,
        height: powerSprite.height,
      } 
    )
  }

  updateSpriteForKeypointId(keyPointId : number, spriteId : number, offsetX : number, offsetY : number, width: number, height: number ){
    const identifier = this.getIdForKeypoint(0, keyPointId);
    //Ensure the sprite exists
    if(!this.renderer.hasSprite(identifier)){
      this.renderer.addSprite({image: this.sprites[spriteId], x: 0, y: 0, width: width, height: height}, identifier)
    }
    //Move sprites to the location of the keypoint
    if(this.currentPose){
      const selectedKeypoint = this.currentPose.keypoints[keyPointId];
      this.renderer.moveSprite(
        identifier,
        selectedKeypoint.x + offsetX,
        selectedKeypoint.y + offsetY
      );
    }
  }

  updateSpriteForTypeAndIndex(type: string, idx : number, spriteId : number, x : number, y : number, width: number, height: number ){
    const identifier = this.getIdForTypeAndIndex(type, idx);
    //Ensure the sprite exists
    if(!this.renderer.hasSprite(identifier)){
      this.renderer.addSprite({image: this.sprites[spriteId], x: x, y: y, width: width, height: height}, identifier)
    } else {
      const sprite = this.renderer.getSprite(identifier);
      this.renderer.moveSprite(identifier, sprite.x, sprite.y);
    }
  }

  getIdForKeypoint(poseIndex: number, keypointIndex: number): string {
    return `pose${poseIndex}point${keypointIndex}`;
  }

  getIdForTypeAndIndex(type: string, idx: number): string {
    return `type${type}point${idx}`;
  }

  playGrab(){
    const grabStartIndex = 1;
    const grabSoundsCount = 3;
    const randomSound = grabStartIndex + Math.floor(Math.random() * grabSoundsCount)
    this.playAudioEffect(randomSound);
  }

  playPlace(){
    const placeStartIndex = 4;
    const placeSoundsCount = 3;
    const randomSound = placeStartIndex + Math.floor(Math.random() * placeSoundsCount)
    this.playAudioEffect(randomSound);
  }

  playJoke(){
    this.playAudioEffect(7);
  }

  playAudioEffect(index: number){
    if(index < this.audioFiles.length){
      this.audioFiles[index].volume = this.volume;
      this.audioFiles[index].play()
    }
  }

}
