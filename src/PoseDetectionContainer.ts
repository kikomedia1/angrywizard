import {
  createDetector, Keypoint,
  movenet,
  MoveNetModelConfig, Pose,
  PoseDetector,
  SupportedModels
} from "@tensorflow-models/pose-detection";
import '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-backend-webgl';
import {Scaler} from "./Scaler.ts";

type PoseCallback = (pose: Pose) => void;
type WarningCallback = (warningMessage: string) => void;

export class PoseDetectionContainer {
  private scaler?: Scaler;
  private videoElement: HTMLVideoElement;
  private detectorPromise: Promise<PoseDetector>;
  private newPosesCallback: PoseCallback;
  private warningCallback: WarningCallback;
  private syntheticPose: Pose = {} as Pose;
  selectedKeypoints: number[] = [0, 5, 6, 9, 10];
  confidenceThreshold: number = 0.1

  constructor(video: HTMLVideoElement, callback?: PoseCallback, warningCallback?: WarningCallback) {
    this.videoElement = video;
    this.newPosesCallback = callback
      ? callback
      : () => console.warn("Pose detected but callback missing");
    this.warningCallback = warningCallback
      ? warningCallback
      : (message: string) => console.warn("No warning channel provided. Message: " + message)
    const detectionModel = SupportedModels.MoveNet;
    const moveNetMultiConfig: MoveNetModelConfig = {
      modelType: movenet.modelType.MULTIPOSE_LIGHTNING,
      modelUrl: "/movenet/model.json"
    };
    console.log("creating detector");
    this.detectorPromise = createDetector(detectionModel, moveNetMultiConfig);
  }
  async startDetection() {
    const detector = await this.detectorPromise;
    console.log("detector functional");
    setInterval(async () => {
      const poses = await detector.estimatePoses(this.videoElement);
      if(poses.length > 1){
        this.warningCallback("More than one player detected! Please stand in the camera angle alone.")
      } else if(poses.length < 1){
        this.warningCallback("No player detected! Please ensure most of your body is visible.")
      } else {
        this.updateSyntheticPose(poses[0])
        this.newPosesCallback(this.syntheticPose);
      }
    }, 50);
  }

  setCallback(callback: PoseCallback){
    this.newPosesCallback = callback;
  }

  setScaler(scaler: Scaler) {
    this.scaler = scaler;
  }

  updateSyntheticPose(pose: Pose):void {
    const multipliers = this.scaler?.resolutionMultipliers;
    for (let i = 0; i < pose.keypoints.length; i++) {
      pose.keypoints[i] = multipliers
        ? this.rescaleKeypoint(pose.keypoints[i], multipliers.width, multipliers.height)
        : pose.keypoints[i];
    }
    for (let i = 0; i < this.selectedKeypoints.length; i++) {
      if(!(pose.keypoints[i].score && pose.keypoints[i].score! > this.confidenceThreshold)){
        pose.keypoints[i] = this.syntheticPose.keypoints
          ? this.syntheticPose.keypoints[i]
          : pose.keypoints[i];
      }
    }
    this.syntheticPose = pose;
  }

  rescaleKeypoint(keypoint: Keypoint, xFactor: number, yFactor: number): Keypoint{
    return {
      ...keypoint,
      x: keypoint.x * xFactor,
      y: keypoint.y * yFactor
    }
  }
}
