export type Resolution = {
  width: number,
  height: number,
}

export class Scaler {

  video: HTMLVideoElement;
  targetResolution: Resolution;
  targetRatio: number;
  videoResolution: Resolution;
  resolutionMultipliers: Resolution;

  constructor(stream: MediaStream, video: HTMLVideoElement, targetResolution: Resolution) {
    this.video = video;
    this.targetResolution = targetResolution;
    this.targetRatio = targetResolution.width / targetResolution.height;
    const streamResolution = stream.getVideoTracks()[0].getSettings();
    this.videoResolution = {
      width: streamResolution.width || 1920,
      height: streamResolution.height || 1080
    }
    this.resolutionMultipliers = {
      width: targetResolution.width / this.videoResolution.width,
      height: targetResolution.height / this.videoResolution.height
    }
    this.enforceVideoResolution()
  }

  private enforceVideoResolution(){
    this.video.width = this.videoResolution.width;
    this.video.height = this.videoResolution.height;
  }

  getResolutionMultipliers(){
    return this.resolutionMultipliers;
  }

}
