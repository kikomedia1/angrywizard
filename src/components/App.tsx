import {useState} from "react";
import {Intro} from "./Intro.tsx";
import {HowToPlay} from "./HowToPlay.tsx";
import {Game} from "./Game.tsx";
import {EndScreen} from "./EndScreen.tsx";

enum AppStates {
  Intro,
  HowTo,
  Playing,
  Finished
}

export function App() {
  const [appState, setAppState] = useState(AppStates.Intro);

  const View = ()=>{
    switch (appState) {
      case AppStates.Intro: return <Intro onNext={()=>{setAppState(AppStates.HowTo)}} />
      case AppStates.HowTo: return <HowToPlay onNext={()=>{setAppState(AppStates.Playing)}} />
      case AppStates.Playing: return <Game onNext={()=>{setAppState(AppStates.Finished)}} />
      case AppStates.Finished: return <EndScreen onNext={()=>{setAppState(AppStates.Playing)}} />
    }
  }
  return <div className={"flex justify-center items-center p-4"}>
    <div>
      <h1 className={"text-2xl font-bold text-green-700 text-center mb-8"} >Angry Wizard</h1>
      <View/>
    </div>
  </div>
}
