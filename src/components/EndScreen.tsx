import {IntroProps} from "./Intro.tsx";

export function EndScreen(props: IntroProps) {
  return <div>
    <button onClick={props.onNext}>Replay?</button>
  </div>
}
