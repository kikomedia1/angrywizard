import {captureCamera} from "../utils.ts";
import {CanvasRenderer} from "../CanvasRenderer.ts";
import {PoseDetectionContainer} from "../PoseDetectionContainer.ts";
import {GameManager} from "../GameManager.ts";
import {Scaler} from "../Scaler.ts";
import {IntroProps} from "./Intro.tsx";
import {useEffect, useRef, useState} from "react";
import {InfoPrompt} from "./InfoPrompt.tsx";

// @ts-ignore
export function Game(props: IntroProps) {
  const renderedOnceRef = useRef(false)
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [timeoutId, setTimeoutId] = useState(0)
  const [warningMsg, setWarningMsg] = useState<string|undefined>()

  const video: HTMLVideoElement = document.getElementById("video") as HTMLVideoElement;
  const poseHead: HTMLImageElement = document.getElementById("poseHead") as HTMLImageElement;
  const poseBody: HTMLImageElement = document.getElementById("poseBody") as HTMLImageElement;
  const leftHand: HTMLImageElement = document.getElementById("leftHand") as HTMLImageElement;
  const rightHand: HTMLImageElement = document.getElementById("rightHand") as HTMLImageElement;
  const leftHandOpen: HTMLImageElement = document.getElementById("leftHandOpen") as HTMLImageElement;
  const rightHandOpen: HTMLImageElement = document.getElementById("rightHandOpen") as HTMLImageElement;
  const background: HTMLImageElement = document.getElementById("background") as HTMLImageElement;
  const elixir1: HTMLImageElement = document.getElementById("elixir1") as HTMLImageElement;
  const elixir2: HTMLImageElement = document.getElementById("elixir2") as HTMLImageElement;
  const elixir3: HTMLImageElement = document.getElementById("elixir3") as HTMLImageElement;
  const elixir4: HTMLImageElement = document.getElementById("elixir4") as HTMLImageElement;
  const poseHeadHappy: HTMLImageElement = document.getElementById("poseHeadHappy") as HTMLImageElement;
  const mouthHappy: HTMLImageElement = document.getElementById("mouthHappy") as HTMLImageElement;
  const mouthAngry: HTMLImageElement = document.getElementById("mouthAngry") as HTMLImageElement;
  const bar: HTMLImageElement = document.getElementById("bar") as HTMLImageElement;
  const power1: HTMLImageElement = document.getElementById("power1") as HTMLImageElement;
  const power2: HTMLImageElement = document.getElementById("power2") as HTMLImageElement;
  const power3: HTMLImageElement = document.getElementById("power3") as HTMLImageElement;
  const power4: HTMLImageElement = document.getElementById("power4") as HTMLImageElement;
  const power5: HTMLImageElement = document.getElementById("power5") as HTMLImageElement;

  const soundtrack: HTMLAudioElement = document.getElementById("soundtrack") as HTMLAudioElement;
  const grab1: HTMLAudioElement = document.getElementById("grab1") as HTMLAudioElement;
  const grab2: HTMLAudioElement = document.getElementById("grab2") as HTMLAudioElement;
  const grab3: HTMLAudioElement = document.getElementById("grab3") as HTMLAudioElement;
  const place1: HTMLAudioElement = document.getElementById("place1") as HTMLAudioElement;
  const place2: HTMLAudioElement = document.getElementById("place2") as HTMLAudioElement;
  const place3: HTMLAudioElement = document.getElementById("place3") as HTMLAudioElement;
  const joke: HTMLAudioElement = document.getElementById("joke") as HTMLAudioElement;


  const handleWarning = (message:string) => {
    if(!warningMsg){
      setWarningMsg(message);
      clearTimeout(timeoutId)
      setTimeoutId(setTimeout(()=>{
        setWarningMsg(undefined);
      }, 2000))
    }
  }

  useEffect(() => {
    if(!renderedOnceRef.current){
      renderedOnceRef.current = true;

      const container = new PoseDetectionContainer(video, undefined, handleWarning);
      container.startDetection();
      captureCamera().then(stream => {
        const scaler = new Scaler(stream, video, {width: 1280, height: 960})
        container.setScaler(scaler);
        const renderer = new CanvasRenderer(scaler, stream, video, canvasRef.current!);
        renderer.renderCanvas();
        const manager = new GameManager(
          renderer,
          container,
          [
            poseHead, //0
            poseBody, 
            leftHand, 
            rightHand, 
            leftHandOpen, 
            rightHandOpen, 
            background, 
            elixir1, //7
            elixir2, 
            elixir3, 
            elixir4, //10
            poseHeadHappy, 
            mouthHappy, 
            mouthAngry, 
            bar, 
            power1, //15
            power2, 
            power3, 
            power4, 
            power5 //19
          ],
          [
            soundtrack,
            grab1,
            grab2,
            grab3,
            place1,
            place2,
            place3,
            joke
          ]
        );
        manager.startGame();
      })
    }
  }, []);

  return <div>
    <canvas ref={canvasRef}/>
    { warningMsg && <InfoPrompt message={warningMsg} /> }
  </div>
}
