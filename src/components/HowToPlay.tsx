import {IntroProps} from "./Intro.tsx";


export function HowToPlay(props: IntroProps) {
  // const [openMirror, setOpenMirror] = useState(false);

  return <div className={"prose"}>
    <h2>How to play?</h2>
    <p>
      Angry wizard has a control scheme based on your Webcam input and the MoveNet AI.
      That means <b>you will move around</b>.
      We don't want you to hurt yourself or any bystander, so please ensure that your surroundings are:
    </p>
    <ul>
      <li>Sufficiently lit (the AI needs to see you)</li>
      <li>Free of any obstacles you could collide with</li>
      <li>Free of any obstacles you could collide with</li>
    </ul>
    <p>We also found out that positioning yourself **1-2 meters** from the camera
      with your head roughly at the center of the view leads to the best experience.
    </p>
    {/*<h3>Need help to get into position?</h3>*/}
    {/*<button onClick={()=>setOpenMirror(true)}>Give me a camera mirror</button>Need help to get into position?*/}
    <h3>All clear?</h3>
    <button  className={"bg-green-600 text-blue-100 text-lg font-bold rounded-lg shadow-lg p-2"} onClick={props.onNext}>Open the game</button>
  </div>
}
