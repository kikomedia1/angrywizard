type InfoPromptProps = {
  message: string,
}

export function InfoPrompt(props: InfoPromptProps) {
  return <div className={"fixed inset-0 flex justify-center items-center"}>
    <div className={"absolute inset-0 bg-slate-600 opacity-20"} />
    <div className={"bg-white shadow-lg rounded-xl p-4 z-20"}>
      <p className={"text-xl font-semibold"}>{props.message}</p>
    </div>
  </div>
}
