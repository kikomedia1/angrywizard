export type IntroProps = {
  onNext: () => void;
}

export function Intro(props: IntroProps){
  return <div className={"prose"}>
    <p>Hello out there and thank you for checking out our GGJ 2024 game!</p>
    <h2>You're in trouble.</h2>
    <p>
      You're playing the wizard's apprentice and you messed up.
      While cleaning, you disposed of a stinky pair of socks that was apparently his favourite one.
      Now he shut you into a cursed room full of animated objects - and he won't let you out until it's clean.
    </p>
    <p>
      How long can you last before an enchanted potion turns you into a goat?
      Or worse... You never know what that grumpy old man is up to.
    </p>
    <h2>Want to give it a shot?</h2>
    <p>You don't need to download or install anything. You just need:</p>
    <ul>
      <li>A Laptop or PC (smartphones don't work all that great, sadly)</li>
      <li>The browser of your choice</li>
      <li>A Webcam</li>
    </ul>
    <button className={"bg-green-600 text-blue-100 text-lg font-bold rounded-lg shadow-lg p-2"} onClick={props.onNext}>Let's get going!</button>
  </div>
}
